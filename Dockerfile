FROM php:7-fpm

RUN apt-get update && apt-get install -y libmcrypt-dev mysql-client \
    && docker-php-ext-install mcrypt pdo_mysql \
    && apt-get install --yes zip unzip php-pclzip

RUN apt-get update && \
    apt-get install -y --no-install-recommends git zip

# Install dependencies
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install -y \
    git \
    nodejs \
    zip
RUN curl -sL https://getcomposer.org/installer | php -- --install-dir /usr/local/bin --filename composer